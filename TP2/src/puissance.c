/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Puissance
 */
#include <stdio.h>

unsigned long puissance(int a, int b); //Prototype de la fonction

int main() {
        unsigned long res; //Utilisation d'un entier sur 32bits non signé car les puissances peut vite atteindre des grandes valeurs
        res = puissance(2,3); //Appel de la fonction
        printf("%lu\n", res);
        return 0;
}

unsigned long puissance(int a, int b) { //Retour de type unsigned long et paramètres de type int
        unsigned long resultat = 1;
        for(int i = 0; i < b; i++) {
                resultat *= a;
        }
        return resultat;
}
