/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Puissance
 */
#include <stdio.h>

int main() {
        int d;
        printf("Saisissez d : ");
        scanf("%d", &d); //Saisie au clavier
        printf("%d\n", ((d & 0x80008) == 0x80008)); //Masque logique du 4eme et 20eme bits vers la gauche et équation booléenne
}
