/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   factorielle
 */
#include <stdio.h>

int factorielle(int n);

int main() {
        int n;
        printf("Saisir n :\n");
        scanf("%d", &n);
        int fact = factorielle(n);
        printf("%d\n", fact );
        return 0;
}

int factorielle (int n) {
        if (n == 0) {
                return 1;
        }
        else {
                return (n * factorielle (n-1)); //Appel récursif
        }
}
