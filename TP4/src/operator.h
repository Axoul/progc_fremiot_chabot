#ifndef OPERATOR_H
#define OPERATOR_H

int somme(int a, int b);
int difference(int a, int b);
int produit(int a, int b);
float quotient(int a, int b);
int modulo(int a, int b);
int et(int a, int b);
int ou(int a, int b);
int negation(int a);

#endif
