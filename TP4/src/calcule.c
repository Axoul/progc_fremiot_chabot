#include <stdio.h>
#include <stdlib.h>
#include "operator.h"

int main(int argc, char **argv) {
        if(argc == 4) {
                char op = argv[1][0]; //Chaine de caractère en char pour le switch ("+\0")
                int num1 = atoi(argv[2]); //Transformation nombre (chaine de caractère) en int
                int num2 = atoi(argv[3]);

                switch(op) { //Selon l'opérateur
                case '+':
                        printf("%d + %d = %d\n", num1, num2, somme(num1, num2));
                        break;

                case '-':
                        printf("%d - %d = %d\n", num1, num2, difference(num1, num2));
                        break;

                case '*':
                        printf("%d * %d = %d\n", num1, num2, produit(num1, num2));
                        break;

                case '/':
                        printf("%d / %d = %f\n", num1, num2, quotient(num1, num2));
                        break;

                case '%':
                        printf("%d modulo %d = %d\n", num1, num2, modulo(num1, num2));
                        break;

                case '&':
                        printf("%d & %d = %d\n", num1, num2, et(num1, num2));
                        break;

                case '|':
                        printf("%d | %d = %d\n", num1, num2, ou(num1, num2));
                        break;

                case '~':
                        printf("%d inversé = %d\n", num1, negation(num1));
                        break;

                default:
                        printf("Opérateur inconnu\n");
                }

        }
        else {
                printf("Erreur de saisie des arguments : ./calcule.out 'op' num1 num2\n");
        }
        return 0;
}
