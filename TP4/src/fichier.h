#ifndef FICHIER_H
#define FICHIER_H

void lireFichier(char *filename);
void ecrireFichier(char *filename, char *message);

#endif
