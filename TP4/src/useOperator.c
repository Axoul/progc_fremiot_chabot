/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Operateurs lib
 */
#include <stdio.h>
#include "operator.h"

int main () {
        int num1, num2;
        char op;
        int c;

        printf("Premier nombre : ");
        scanf("%d", &num1); //Saisie au clavier
        while((c=getchar()) != EOF && c != '\n'); //Vidage du buffer
        printf("Second nombre : ");
        scanf("%d", &num2);
        while((c=getchar()) != EOF && c != '\n');
        printf("Opérateur : ");
        scanf("%c", &op);
        while((c=getchar()) != EOF && c != '\n');

        switch(op) { //Selon l'opérateur
        case '+':
                printf("%d + %d = %d\n", num1, num2, somme(num1, num2));
                break;

        case '-':
                printf("%d - %d = %d\n", num1, num2, difference(num1, num2));
                break;

        case '*':
                printf("%d * %d = %d\n", num1, num2, produit(num1, num2));
                break;

        case '/':
                printf("%d / %d = %f\n", num1, num2, quotient(num1, num2));
                break;

        case '%':
                printf("%d modulo %d = %d\n", num1, num2, modulo(num1, num2));
                break;

        case '&':
                printf("%d & %d = %d\n", num1, num2, et(num1, num2));
                break;

        case '|':
                printf("%d | %d = %d\n", num1, num2, ou(num1, num2));
                break;

        case '~':
                printf("%d inversé = %d\n", num1, negation(num1));
                break;

        default:
                printf("Opérateur inconnu\n");
        }
        return 0;
}
