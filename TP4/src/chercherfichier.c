#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int cptLines(void);
int cptChar(void);
int searchLine(char *line, char *motRecherche);

FILE *fichier = NULL; //Pointeur de fichier

int main(int argc, char **argv) {
        if(argc == 3) { //Si les arguments sont valides
                char *mot = argv[1];
                char *filename = argv[2];
                char line = 1;
                fichier = fopen(filename, "r"); //Ouverture du fichier en lecture
                int nbLines = cptLines();
                char *currentLine;
                int occurence;
                while(line <= nbLines) { //scrutation de toute les lignes une par une
                        int nbChar = cptChar()+1; //Comptage du nombre de caractères par ligne + \0
                        currentLine = malloc(nbChar * sizeof *currentLine); //Allocation dynamique de la ligne dans une chaine créée à sa taille
                        fgets(currentLine, nbChar, fichier); //Copie de la ligne dans la chaine de caractère
                        occurence = searchLine(currentLine, mot); //Appel pour compter le nombre d'occurence dans la ligne
                        free(currentLine); //Supression de la variable dynamique
                        fseek(fichier, 1, SEEK_CUR); //Positionnement du curseur à la ligne suivante (apres le \n)
                        if(occurence > 0) {
                                printf("Ligne %d, %d fois\n", line, occurence);
                        }
                        line++;
                }
                fclose(fichier); //Fermeture du fichier
        }
        else {
                printf("Erreur de saisie des arguments : ./chercherfichier.out mot_recherche nom_fichier\n");
        }
        return 0;
}


int cptLines(void) { //Revoi le nombre de lignes dans le ficher
        int lines = 0, c;
        while((c=fgetc(fichier)) != EOF) { //Jusqu'à la fin du fichier
                if(c=='\n') { //A chaque daut de ligne
                        lines++;
                }
        }
        fseek(fichier, 0, SEEK_SET); //Repositionnement du curseur au début du fichier
        return lines;
}


int cptChar(void) { //Renvoi le nombre de caractère dans la ligne
        int nb = 0, c;
        while((c=fgetc(fichier)) != '\n') {
                nb++;
        }
        fseek(fichier, -(nb+1), SEEK_CUR); //Repositionnement du curseur au début de la ligne actuelle
        return nb;
}


int searchLine(char *line, char *motRecherche) {
        int count = 0;
        while(line = strstr(line, motRecherche)) { //Cherche une chaine de caratère dans une autre
                count++;
                line++; //Avance après la découverte du mot
        }
        return count;
}
