#include <stdio.h>
#include <stdlib.h>

void lireFichier(char *filename) {
        FILE* fichier = NULL; //Pointeur
        fichier = fopen(filename, "r"); //Ouverture du fichier en lecture seulement
        if(fichier != NULL) {
                char caractere;
                do
                {
                        caractere = fgetc(fichier); //Lecture caractere
                        printf("%c", caractere); //Affichage
                } while(caractere != EOF); //Tant que ce n'est pas la fin de fichier (EOF)

                fclose(fichier);
        }
        else {
                printf("Fichier vide ou inexistant\n");
        }
}

void ecrireFichier(char *filename, char *message) {
        FILE* fichier = NULL;
        fichier = fopen(filename, "a"); //Ouverture du fichier en mode écriture à la fin
        if(fichier != NULL) {
              fprintf(fichier, "%s", message); //Printf dans un fichier
              printf("Ecriture réussie !\n");
              fclose(fichier);
        }
        else {
              printf("Impossible d'ouvrir le fichier !\n");
        }
}
