#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fichier.h"

typedef struct Eleve Eleve; //Nouveau type défini par la structure Eleve
struct Eleve { //Structure
        char nom[20]; //Taille définie quantitativement. Un malloc aurait été possible et mieux compatible avec le scanf car char *
        char prenom[20];
        char adresse[50];
        int note1;
        int note2;
};

void emptyBuffer(void);
void removeNewLine(char *str);

int main() {
        int i = 0;
        Eleve eleves[5];
        for(i = 0; i < 5; i++ ) {
                printf("\nNom élève n°%d : ", i+1);
                scanf("%19[^\n]s", &eleves[i].nom); //Tient compte des espaces et peut avoir au max 19 char
                emptyBuffer();

                printf("\nPrénom élève n°%d : ", i+1);
                scanf("%19[^\n]s", &eleves[i].prenom);
                emptyBuffer();

                printf("\nAdresse élève n°%d : ", i+1);
                scanf("%49[^\n]s", &eleves[i].adresse);
                emptyBuffer();

                printf("\n1ère note élève n°%d : ", i+1);
                scanf("%2d", &eleves[i].note1);
                emptyBuffer();

                printf("\n2ème note élève n°%d : ", i+1);
                scanf("%2d", &eleves[i].note2);
                emptyBuffer();
        }

        char *line;
        int size;
        for(i = 0; i < 5; i++) {
                size = strlen(eleves[i].nom)+strlen(eleves[i].prenom)+strlen(eleves[i].adresse)+4+8+2; //4 : notes; 8 : , et espaces, 2 : \n et \0
                line = malloc(size * sizeof *line); //Allocation dynamique de la variable de sortie
                sprintf(line, "%s, %s, %s, %d, %d\n", eleves[i].nom, eleves[i].prenom, eleves[i].adresse, eleves[i].note1, eleves[i].note2); //Printf dans une string
                ecrireFichier("DB", line); //Ecriture de la ligne dans le fichier DB
                free(line); //Supression de l'allocation
        }
        return 0;
}

void emptyBuffer(void) { //Permet de vider le buffer stdin pour le prochain scanf
      char c;
      while ((c = getchar()) != '\n' && c != EOF);
}

void removeNewLine(char *str) { //Remplace le \n par un \0
        char *ptr;
        if((ptr = strchr(str, '\n')) != NULL) {
                *ptr = '\0';
        }
}
