#ifndef REPERTOIRE_H
#define REPERTOIRE_H

void lireDossier(char *repertoire);
void lireDossierReccursif(char *repertoire);
void lireDossierIteratif(char *repertoire);

#endif
