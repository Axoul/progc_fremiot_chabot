int somme(int a, int b) {
        return a + b;
}

int difference(int a, int b) {
        return a - b;
}

int produit(int a, int b) {
        return a * b;
}

float quotient(int a, int b) {
        return (float)a / (float)b; //Cast pour le type de retour
}

int modulo(int a, int b) {
        return a % b;
}

int et(int a, int b) {
        return a & b;
}

int ou(int a, int b) {
        return a | b;
}

int negation(int a) {
        return ~a;
}
