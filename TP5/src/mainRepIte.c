/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Parcours répertoire itératif
 */
#include <stdio.h>
#include "repertoire.h"

int main(int argc, char **argv) {
        if(argc == 2) {
                char *dossier = argv[1];
                lireDossierIteratif(dossier);
        }
        else {
                printf("Utilisez cette syntaxe : ./mainRep.out dossier\n");
        }
        return 0;
}
