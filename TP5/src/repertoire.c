#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

void lireDossier(char *repertoire) {
        DIR *dirp = opendir(repertoire); //Ouverture de dossier en suivant le chemin repertoire
        if (dirp != NULL) { //Repertoire non vide
                struct dirent *ent;
                while(1) {
                        ent = readdir(dirp); //Informations sur le fichier ou dossier courant
                        if(ent == NULL) {
                                break;
                        }
                        printf("%s\n", ent->d_name); //(*ent).d_name
                }
                closedir(dirp); //Fermeture du dossier
        }
        else {
                printf("Répertoire inexistant\n");
        }
}

void lireDossierReccursif(char *repertoire) {
        DIR *dirp = opendir(repertoire);
        if (dirp != NULL) {
                struct dirent *ent;
                while(1) {
                        ent = readdir(dirp);
                        if (ent == NULL) {
                                break;
                        }
                        if (strcmp(ent->d_name,".") == 0 || strcmp(ent->d_name,"..") == 0) { //Eviter le . (repertoire courant) ou ..(répertoire parent)
                                continue; //Saute l'itération
                        }
                        if (ent->d_type == DT_DIR) { //Si c'est un dossier
                                char courant[100] = ""; //Buffer
                                sprintf(courant, "%s/%s", repertoire, ent->d_name);
                                printf("\nDOSSIER : %s\n", courant); //(*ent).d_name
                                lireDossierReccursif(courant); //Appel récursif
                                printf("\n");
                        }
                        else {
                                printf("%s\n", ent->d_name); //(*ent).d_name
                        }
                }
                closedir(dirp);
        }
        else {
                printf("Répertoire inexistant\n");
        }
}


void lireDossierIteratif(char *repertoire) {
        DIR *dirp = opendir(repertoire);
        struct dirent* ent = NULL;
        int n = 0; //Incrément de sous dossiers
        unsigned int i = 0;
        do {
                ent = readdir(dirp);
                //printf("repertoire vaut maintenant \"%s\"\n", repertoire);
                if(ent != NULL) {
                        //printf("non\n");
                        printf("Nom : \"%s\"\n", ent->d_name);   //On affiche le nom du fichier

                        if((ent->d_type == DT_DIR)&&(strcmp(ent->d_name,".") != 0)&&(strcmp(ent->d_name,"..")!=0)) { //Si c'est un sous dossier
                                strcat(repertoire, "/");
                                strcat(repertoire, ent->d_name); //Mofification du chemin par ce sous dossier
                                printf("repertoire vaut maintenant \"%s\"\n", repertoire);
                                dirp = opendir(repertoire); //Ouverture
                                n++; //Incrément de sous dossier
                                printf("n vaut donc : %d\n", n);
                        }
                }

                if((ent == NULL) && (n != 0)) { //Si le sous dossier est vide
                        printf("! Remontee dans le repertoire !\n");
                        n--;
                        printf("n vaut donc : %d\n", n);
                        i = strlen(repertoire);
                        while(repertoire[i-1] != '/') {
                                i--; //Position du premier / apres le dernier sous dossier
                        }
                        printf("La taille que repertoire doit reprendre est : %d\n", i);
                        //strncat(repertoire, "", i);
                        repertoire[i-1] = '\0'; //Dossier parent
                        printf("repertoire vaut maintenant 2 : \"%s\"\n", repertoire);
                        dirp = opendir(repertoire); //Ouverture du dossier parent
                }

        }
        while((ent != NULL) || n != 0);

        closedir(dirp);
}
