/* Fichier: serveur.c
 * Communication client-serveur
 * Auteurs: John Samuel, ...
 */


#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "serveur.h"
#include "operator.h"

/* renvoyer un message (*data) au client (client_socket_fd)
 */
int renvoie_message(int client_socket_fd, char *data) {
        int data_size = write (client_socket_fd, (void *) data, strlen(data));

        if (data_size < 0) {
                perror("erreur ecriture");
                return(EXIT_FAILURE);
        }
}

/* accepter la nouvelle connection d'un client et lire les données
 * envoyées par le client. En suite, le serveur envoie un message
 * en retour
 */
int recois_envoie_message(int socketfd) {
        struct sockaddr_in client_addr;
        char data[1024];

        int client_addr_len = sizeof(client_addr);

        // nouvelle connection de client
        int client_socket_fd = accept(socketfd, (struct sockaddr *) &client_addr, &client_addr_len);
        if (client_socket_fd < 0 ) {
                perror("accept");
                return(EXIT_FAILURE);
        }

        memset(data, 0, sizeof(data));

        //lecture de données envoyées par un client
        int data_size = read (client_socket_fd, (void *) data, sizeof(data));

        if (data_size < 0) {
                perror("erreur lecture");
                return(EXIT_FAILURE);
        }

        printf ("Message recu: %s\n", data);
        char code[10];
        sscanf(data, "%s:", code);

        //Si le message commence par le mot: 'message:'
        if (strcmp(code, "message:") == 0) {
                printf("Votre réponse (max 1000 caracteres): "); //Modification pour réponse
                fgets(data, 1024, stdin); //Modification pour réponse
                renvoie_message(client_socket_fd, data);
        }

        //fermer le socket
        close(socketfd);
}

int recois_numeros_calcule(int socketfd) {
        struct sockaddr_in client_addr;
        char data[1024];

        int client_addr_len = sizeof(client_addr);

        // nouvelle connection de client
        int client_socket_fd = accept(socketfd, (struct sockaddr *) &client_addr, &client_addr_len);
        if (client_socket_fd < 0 ) {
                perror("accept");
                return(EXIT_FAILURE);
        }

        memset(data, 0, sizeof(data));

        //lecture de données envoyées par un client
        int data_size = read (client_socket_fd, (void *) data, sizeof(data));

        if (data_size < 0) {
                perror("erreur lecture");
                return(EXIT_FAILURE);
        }

        printf ("Message recu: %s\n", data);
        char code[15];
        sscanf(data, "%s", code);
        //Si le message commence par le mot: 'calcule:'
        if (strcmp(code, "calcule:") == 0) {
                char op;
                int num1, num2;
                sscanf(data, "calcule: %c %d %d", &op, &num1, &num2);
                switch(op) { //Selon l'opérateur
                case '+':
                        sprintf(data, "calcule: %d", somme(num1, num2));
                        break;

                case '-':
                        sprintf(data, "calcule: %d", difference(num1, num2));
                        break;

                case '*':
                        sprintf(data, "calcule: %d", produit(num1, num2));
                        break;

                case '/':
                        sprintf(data, "calcule: %f", quotient(num1, num2));
                        break;

                case '%':
                        sprintf(data, "calcule: %d", modulo(num1, num2));
                        break;

                case '&':
                        sprintf(data, "calcule: %d", et(num1, num2));
                        break;

                case '|':
                        sprintf(data, "calcule: %d", ou(num1, num2));
                        break;

                case '~':
                        sprintf(data, "calcule: %d", negation(num1));
                        break;

                default:
                        printf("Opérateur inconnu\n");
                        sprintf(data, "calcule: ERROR");
                }
                renvoie_message(client_socket_fd, data);
        }

        //fermer le socket
        close(socketfd);
}


int main(int argc, char **argv) {
        if(argc == 2) {
                int socketfd;
                int bind_status;
                int client_addr_len;

                struct sockaddr_in server_addr, client_addr;

                /*
                 * Creation d'un socket
                 */
                socketfd = socket(AF_INET, SOCK_STREAM, 0);
                if ( socketfd < 0 ) {
                        perror("Unable to open a socket");
                        return -1;
                }

                int option = 1;
                setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

                //détails du serveur (adresse et port)
                memset(&server_addr, 0, sizeof(server_addr));
                server_addr.sin_family = AF_INET;
                server_addr.sin_port = htons(PORT);
                server_addr.sin_addr.s_addr = INADDR_ANY;

                bind_status = bind(socketfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
                if (bind_status < 0 ) {
                        perror("bind");
                        return(EXIT_FAILURE);
                }

                listen(socketfd, 10);
                if(strcmp(argv[1], "message")==0) {
                        recois_envoie_message(socketfd);
                }
                else if(strcmp(argv[1], "calcule")==0) {
                        recois_numeros_calcule(socketfd);
                }
        }
        else {
                printf("Problème arguments\n");
        }

        return 0;
}
