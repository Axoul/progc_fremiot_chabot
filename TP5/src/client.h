#ifndef __CLIENT_H__
#define __CLIENT_H__

#define PORT 8089

typedef struct Etudiant Etudiant; //Nouveau type défini par la structure Eleve
struct Etudiant { //Structure
        int notes[5];
};

/* envoi et reception de message
 */
int envoie_recois_message(int socketfd);
int envoie_operateur_numeros(int socketfd);

#endif
