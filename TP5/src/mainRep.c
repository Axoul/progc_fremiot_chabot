/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Parcours répertoire
 */
#include <stdio.h>
#include "repertoire.h"

int main(int argc, char **argv) {
        if(argc == 2) {
                char *dossier = argv[1];
                lireDossier(dossier);
        }
        else {
                printf("Utilisez cette syntaxe : ./mainRep.out dossier\n");
        }
        return 0;
}
