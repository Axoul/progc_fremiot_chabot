#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "client.h"

Etudiant etudiants[5];
/* envoi et reception de message
 */
int envoie_recois_message(int socketfd) {
        char data[1024];
        memset(data, 0, sizeof(data));
        char message[100];
        printf("Votre message (max 1000 caracteres): ");
        fgets(message, 1024, stdin);
        strcpy(data, "message: ");
        strcat(data, message);

        int write_status = write(socketfd, data, strlen(data));
        if ( write_status < 0 ) {
                perror("erreur ecriture");
                exit(EXIT_FAILURE);
        }

        memset(data, 0, sizeof(data));
        int read_status = read(socketfd, data, sizeof(data));
        if ( read_status < 0 ) {
                perror("erreur lecture");
                return -1;
        }

        printf("Message recu: %s\n", data);

        return 0;
}

int envoie_operateur_numeros(int socketfd) {
        char data[1024];
        memset(data, 0, sizeof(data));
        char message[100];
        printf("Votre calcul (op nb1 nb2): ");
        fgets(message, 1024, stdin);
        strcpy(data, "calcule: ");
        strcat(data, message);

        int write_status = write(socketfd, data, strlen(data));
        if ( write_status < 0 ) {
                perror("erreur ecriture");
                exit(EXIT_FAILURE);
        }

        memset(data, 0, sizeof(data));
        int read_status = read(socketfd, data, sizeof(data));
        if ( read_status < 0 ) {
                perror("erreur lecture");
                return -1;
        }

        printf("%s\n", data);

        return 0;
}

void readFiles(void) {
        for(int i=1, i)
        FILE* fichier = NULL; //Pointeur
        fichier = fopen(filename, "r"); //Ouverture du fichier en lecture seulement
        if(fichier != NULL) {
                int note;
                fscanf(fichier, "%d", note);
                fclose(fichier);
        }
        else {
                printf("Fichier vide ou inexistant\n");
        }
}


int main(int argc, char **argv) {
        if(argc == 2) {
                int socketfd;
                int bind_status;

                struct sockaddr_in server_addr, client_addr;

                /*
                 * Creation d'un socket
                 */
                socketfd = socket(AF_INET, SOCK_STREAM, 0);
                if ( socketfd < 0 ) {
                        perror("socket");
                        exit(EXIT_FAILURE);
                }

                //détails du serveur (adresse et port)
                memset(&server_addr, 0, sizeof(server_addr));
                server_addr.sin_family = AF_INET;
                server_addr.sin_port = htons(PORT);
                server_addr.sin_addr.s_addr = INADDR_ANY;

                //demande de connection au serveur
                int connect_status = connect(socketfd, (struct sockaddr *) &server_addr, sizeof(server_addr));
                if ( connect_status < 0 ) {
                        perror("connection serveur");
                        exit(EXIT_FAILURE);
                }
                //Selon l'argument de fonctionnement
                if(strcmp(argv[1], "message")==0) {
                        envoie_recois_message(socketfd);
                }
                else if(strcmp(argv[1], "calcule")==0) {
                        envoie_operateur_numeros(socketfd);
                }
                close(socketfd);
        }
        else {
                printf("Problème arguments\n");
        }
}
