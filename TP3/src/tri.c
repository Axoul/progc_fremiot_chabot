/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Tri
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

int findMin(int *var, int bornemin);

int size = 100;

int main() {
        int entiers[size];
        srand(time(NULL));
        int i, tmp, dest;

        for(i = 0; i < size; i++) {
                entiers[i] = rand(); //Remplissage
        }

        for(i = 0; i < size; i++) { //Echange de place avec le ième et le plus petit des autres
                dest = findMin(entiers, i);
                tmp = entiers[i];
                entiers[i] = entiers[dest]; //Echange
                entiers[dest] = tmp;
        }

        printf("100 entiers triés : ");
        for(i = 0; i < size; i++) {
                printf("%d ", entiers[i]); //Affichage
        }
        printf("\n");
        return 0;

}

int findMin(int *var, int bornemin) { //Fonction pour trouver le plus petit int d'une liste avec une borne min
        int min = INT_MAX, indice;
        for(int i = bornemin; i < size; i++) {
                if(var[i] < min) {
                        min = var[i];
                        indice = i;
                }
        }
        return indice;
}
