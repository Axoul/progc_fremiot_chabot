/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Couleurs compteur
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct RGBA RGBA;
struct RGBA { //Initialisation structure
        unsigned char red;
        unsigned char green;
        unsigned char blue;
        unsigned char alpha;
};

void fillColors(RGBA in[]); //Structure en paramètre
void compareColors(RGBA in[]);

int size = 100;
int range = 64;

int main() {
        RGBA couleurs[size];
        srand(time(NULL));
        fillColors(couleurs);
        compareColors(couleurs);
        return 0;
}

void fillColors(RGBA in[]) {
        for(int i = 0; i < size-1; i++) {
                in[i].red = (unsigned char)rand()/range;
                in[i].green = (unsigned char)rand()/range; //Remplissage des couleurs avec peu de valeurs possible pour avoir plusieurs lignes pareils
                in[i].blue = (unsigned char)rand()/range;
                in[i].alpha = (unsigned char)rand()/range;
        }
}

void compareColors(RGBA in[]) {
        int present[100] = {0}; //Flag par indice de présence
        for(int i = 0; i < size; i++) {
                if(present[i] == 0) { //Pour éviter les doublons
                        int count = 1;
                        for(int j = i+1; j < size; j++) { //Double scrutation
                                if((in[i].red == in[j].red)&&(in[i].green == in[j].green)&&(in[i].blue == in[j].blue)&&(in[i].alpha == in[j].alpha)) {
                                        present[j] = 1;
                                        count++;
                                }
                        }
                        printf("R:0x%.2x|G:0x%.2x|B:0x%.2x|A:0x%.2x|Cpt:%d\n", in[i].red, in[i].green, in[i].blue, in[i].alpha, count); //Affichage en hexa
                }
        }
}
