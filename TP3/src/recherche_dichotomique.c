/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Recherche Dichotomique
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

int findMin(int var[], int bornemin);
int rechercheDichotomique(int elem, int liste[], int borneinf, int bornesup);

int size = 100;

int main() {
        int entiers[size];
        srand(time(NULL));
        int i, tmp, dest;

        for(i = 0; i < size; i++) {
                entiers[i] = rand(); //Remplissage
        }

        for(i = 0; i < size; i++) {
                dest = findMin(entiers, i);
                tmp = entiers[i];
                entiers[i] = entiers[dest];       //Tri croissant
                entiers[dest] = tmp;
        }
        printf("Les %d entiers : ",size);
        for(i = 0; i < size; i++) {
                printf("%d ", entiers[i]); //Affichage
        }
        printf("\nVeuillez entrer un entier :");
        int saisie;
        scanf("%d", &saisie); //Saisie
        int resultat = rechercheDichotomique(saisie, entiers, 0, size); //Appel de la fonction récursive
        printf("L'entier %d est présent en indice %d\n", saisie, resultat+1);
        return 0;

}

int findMin(int var[], int bornemin) { //Fonction de tri
        int min = INT_MAX, indice;
        for(int i = bornemin; i < size; i++) {
                if(var[i] < min) {
                        min = var[i];
                        indice = i;
                }
        }
        return indice;
}

int rechercheDichotomique(int elem, int liste[], int borneinf, int bornesup) {
        int m = (borneinf+bornesup)/2; //Calcul taille de la nouvelle liste
        if(liste[m] == elem) { //Si trouvé
                return m;
        }
        if(liste[m] > elem) {
                return rechercheDichotomique(elem, liste, borneinf, (m-1)); //Appel récursif en changeant les bornes de la liste
        }
        else {
                return rechercheDichotomique(elem, liste, (m+1), bornesup); //Idem mais sur l'autre moitié
        }
}
