/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Octets
 */
#include <stdio.h>
#include <limits.h>
#include <float.h> //Idem que limits.h mais les les nombres décimaux

int main() {
        int i;

        short var1 = 0xabcd;
        int var2 = 0x12345678;
        long int var3 = LONG_MAX/2;     //Initialisation avec valeurs choisies et cohérantes
        float var4 = FLT_MAX/2.8;
        double var5 = DBL_MAX/11.8;
        long double var6 = LDBL_MAX	/ 32.78;

        unsigned char *ptr1 =NULL;
        unsigned char *ptr2 =NULL;
        unsigned char *ptr3 =NULL; //Initialisation des pointeurs
        unsigned char *ptr4 =NULL;
        unsigned char *ptr5 =NULL;
        unsigned char *ptr6 =NULL;
        ptr1 = (unsigned char*)&var1;
        ptr2 = (unsigned char*)&var2;
        ptr3 = (unsigned char*)&var3;
        ptr4 = (unsigned char*)&var4;
        ptr5 = (unsigned char*)&var5;
        ptr6 = (unsigned char*)&var6;

        printf("Octets short : ");
        for(i = (int)(sizeof(short))-1; i >= 0 ; i--) {     //Déroulement inverse des octets selon la taille du type de variable
                printf("0x%x ", *(ptr1 + i));
        }

        printf("\nOctets int : ");
        for(i = (int)(sizeof(int))-1; i >= 0 ; i--) {
                printf("0x%x ", *(ptr2 + i));
        }

        printf("\nOctets long int : ");
        for(i = (int)(sizeof(long int))-1; i >= 0 ; i--) {
                printf("0x%x ", *(ptr3 + i));
        }

        printf("\nOctets float : ");
        for(i = (int)(sizeof(float))-1; i >= 0 ; i--) {
                printf("0x%x ", *(ptr4 + i));
        }

        printf("\nOctets double : ");
        for(i = (int)(sizeof(double))-1; i >= 0 ; i--) {
                printf("0x%x ", *(ptr5 + i));
        }

        printf("\nOctets long double : ");
        for(i = (int)(sizeof(long double))-1; i >= 0 ; i--) {
                printf("0x%x ", *(ptr6 + i));
        }
        printf("\n");
        return 0;
}
