/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Chercher entiers
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
        int size = 100;
        int entiers[size];
        srand(time(NULL));
        int i = 0;
        for(i = 0; i < size; i++) {
                entiers[i] = rand(); //Remplissage
        }
        printf("Les %d entiers : ",size);
        for(i = 0; i < size; i++) {
                printf("%d ", entiers[i]); //Affichage
        }
        printf("\n");
        int ret, flag = 0;
        printf("Veuillez rentrer un entier : ");
        scanf("%d",&ret); //Saisie
        for(i = 0; i < size; i++) {
                if(entiers[i] == ret) {
                        printf("L'entier %d est présent en %d position\n", entiers[0], i+1);
                        flag = 1; //Pour signaler le cas où aucun n'est égal
                }
        }
        if(!flag) { //Pour éviter flag==0
                printf("L'entier %d n'est pas présent !\n", ret);
        }
        return 0;
}
