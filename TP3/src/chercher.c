/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Chercher chaine caractère
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void fillTableau(char **in, int w, int l);
void printTableau(char **in, int w);
int compareTableau(char **in, char elem[], int w, int l);

int main() {
        int word = 10, letter = 6, i;
        char **tableau; //Création du pointeur
        char entree[letter+1];
        srand(time(NULL));
        tableau = malloc(word * sizeof *tableau); //Allocation dynamique du tableau
        for(i = 0; i < word; i++) {
                tableau[i] = malloc((letter+1) * sizeof *tableau[i]); //Création de tout les indices
        }
        fillTableau(tableau, word, letter);
        printTableau(tableau, word);
        printf("Indiquez la phrase a retrouver (6 lettres max) :\n");
        fgets(entree, (letter+1), stdin); //Saisie clavier plus simple pour un string avec fgets
        int ret = compareTableau(tableau, entree, word, letter);
        if(ret == -1) {
                printf("Il n'y a pas de correspondance\n");
        }
        else {
                printf("%s est présent en %d position\n", entree, ret+1);
        }
        for(i = 0; i < word; i++) {
                free(tableau[i]); //Supression des variables allouées
        }
        free(tableau);
        return 0;
}

void fillTableau(char **in, int w, int l) {
        for(int i = 0; i < w; i++) {
                for(int j = 0; j < l; j++) {
                        in[i][j] = 'a' + (rand() % 26); //Random de lettre minuscules (ascii)
                }
                in[i][6] = '\0'; //Fin chaine de caractères
        }
}

void printTableau(char **in, int w) { //Affichage
        for(int i = 0; i < w; i+=5) {
                printf("%s | %s | %s | %s | %s\n", in[i], in[i+1], in[i+2], in[i+3], in[i+4]);
        }
}

int compareTableau(char **in, char elem[], int w, int l) {
        for(int i = 0; i < w; i++) { //Scrutation des différents mots
                int k = 0;
                for(int j = 0; j < l; j++) { //Scrutation de chaques lettres
                        if(elem[j] == in[i][j]) {
                                k++;
                        }
                }
                if(k == l) {
                        return i;
                }
        }
        return -1;
}
