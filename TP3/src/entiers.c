/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   100 entiers
 */
#include <stdio.h>
#include <stdlib.h> //Pour rand()
#include <time.h>
#include <limits.h> //Pour initialiser min avec le plus grand int

int main() {
        int size = 100, min = INT_MAX, max = 0;
        int entiers[size];
        srand(time(NULL)); //Seed rand()
        int i;
        for(i = 0; i < size; i++) {
                entiers[i] = rand(); //Remplissage
        }
        for(i = 0; i < size; i++) {
                if(entiers[i] > max) {
                        max = entiers[i]; //Compare avec le dernier plus grand
                }
                if(entiers[i] < min) {
                        min = entiers[i]; //Compare avec le dernier plus petit
                }
        }
        printf("Les %d entiers : ",size);
        for(i = 0; i < size; i++) { //Affichage
                printf("%d ", entiers[i]);
        }
        printf("\nLe plus grand : %d | Le plus petit : %d\n",max, min);
}
