/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   64 bits
 */
#include <stdio.h>

int main() {
        printf("%lu octets | %lu octets | %lu octets | %lu octets | %lu octets | %lu octets | %lu octets | %lu octets | %lu octets\n", sizeof(int), sizeof(int*), sizeof(int**), sizeof(char*), sizeof(char**), sizeof(char***), sizeof(float*), sizeof(float**), sizeof(float***));
        return 0;
}
