/*
   FREMIOT Théo
   CHABOT Axel
   4ETI Groupe D
   Triangle rectangle avec boucle while (même fonctionnement)
 */
#include <stdio.h>

int main() {
        int compter;
        int c;
        printf("Indiquez la taille du triangle : ");
        scanf("%d", &compter);
        while((c=getchar()) != EOF && c != '\n'); //Vidage du buffer
        
        int i = 0;
        while(i < compter) {
                if(i == 0) {
                        printf("*\n");
                }

                else if(i == 1) {
                        printf("**\n");
                }

                else if(i == compter-1) {
                        int j = 0;
                        while(j < compter) {
                                printf("*");
                                j++;
                        }
                        printf("\n");
                }

                else {
                        printf("*");
                        int j = 0;
                        while(j < i-1) {
                                printf("#");
                                j++;
                        }
                        printf("*\n");
                }
                i++;
        }
        return 0;
}
